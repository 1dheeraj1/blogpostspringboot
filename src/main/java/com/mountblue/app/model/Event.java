package com.mountblue.app.model;

import javax.persistence.*;
import java.time.LocalDate;
@Entity
@Table
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;
    private String description;
    private int durationUnit;
    private LocalDate eventCreatedAt;
    private int eventLife;
    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH,CascadeType.DETACH})
    @JoinColumn(name="user_id")
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(int durationUnit) {
        this.durationUnit = durationUnit;
    }

    public LocalDate getEventCreatedAt() {
        return eventCreatedAt;
    }

    public void setEventCreatedAt(LocalDate eventCreatedAt) {
        this.eventCreatedAt = eventCreatedAt;
    }

    public int getEventLife() {
        return eventLife;
    }

    public void setEventLife(int eventLife) {
        this.eventLife = eventLife;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}
