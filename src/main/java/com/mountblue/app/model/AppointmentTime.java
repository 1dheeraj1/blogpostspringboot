package com.mountblue.app.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table
public class AppointmentTime {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

   private LocalDate date;

   private int time;

   @ManyToOne(cascade =  {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH,CascadeType.DETACH})
   @JoinColumn(name="user_id")
   private User user;


   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public LocalDate getDate() {
      return date;
   }

   public void setDate(LocalDate date) {
      this.date = date;
   }

   public int getTime() {
      return time;
   }

   public void setTime(int time) {
      this.time = time;
   }

   public User getUser() {
      return user;
   }

   public void setUser(User user) {
      this.user = user;
   }
}
