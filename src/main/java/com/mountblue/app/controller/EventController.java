package com.mountblue.app.controller;

import com.mountblue.app.model.Event;
import com.mountblue.app.model.User;
import com.mountblue.app.service.EventService;
import com.mountblue.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/event")
public class EventController {

    @Autowired
    EventService eventService;
    @Autowired
    UserService userService;

    @GetMapping("/create/{userId}")
    public String eventCreate(@PathVariable("userId") int userId, Model model)
    {
        Event event=new Event();
        model.addAttribute("userId",userId);
        model.addAttribute("event",event);
        return "eventForm";
    }
    @GetMapping("/insert")
    public String eventInsert(@RequestParam("userId") int userId, @ModelAttribute("event")Event event,Model model)
    {
        Optional<User> optional =userService.findById(userId);
        User user=optional.get();
        event.setEventCreatedAt(LocalDate.now());
        user.addEvent(event);
        userService.saveUser(user);
        return "redirect:/dashboard/"+userId;
    }

}
